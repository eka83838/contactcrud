let OSS = require('ali-oss');

export const API_URL = "https://simple-contact-crud.herokuapp.com/"
export const API_TIMEOUT = 120000;

//-------------//--------------- Image Validate
export const NOTE_IMAGE_VALIDATE = "(File hanya bisa jpg/jpeg/png dan ukuran file max 2 mb)"
export const NOTE_IMAGE_SIZE = 2048
export const NOTIFY_IMAGE_SIZE = "Size file maximal 2 mb"
export const NOTIFY_IMAGE_TYPE = "Tipe file hanya boleh PNG/JPG/JPEG"
//-------------//----------------

export const DEFAULT_DATEPICKER = new Date(new Date().setFullYear(new Date().getFullYear() - 25)).toISOString().split('T')[0]

// configuration client OSS