import { regexSymbolNum, regexNumOnly, imageTypeValidate, imageSizeValidate, } from '../../config/utils'
import { NOTIFY_IMAGE_SIZE, NOTIFY_IMAGE_TYPE } from '../constant'

export const EditContactVal = (values) => {
    const errors = {}

    if (!values.id){
        errors.id = 'Id harus diisi'
    }

    if (!values.firstName){
      errors.firstName = 'First Name harus diisi'
    }else{
      if (values.firstName) {
        if (regexSymbolNum(values.firstName)) {
          errors.firstName = 'First Name diisi huruf alfabet'
        }
      }
    }

    if (!values.lastName){
      errors.lastName = 'Last Name harus diisi'
    } else {
      if(values.lastName) {
        if (regexSymbolNum(values.lastName)) {
          errors.lastName = 'Last Name diisi huruf alfabet'
        }
      }
    }
    
    if (!values.age){
      errors.age = 'Age harus diisi'
    } else {
      if (regexNumOnly(values.age)) {
        errors.age = 'Age harus diisi angka'
      }else if (values.age > 100) {
        errors.age = 'Age harus diisi kurang dari 100'
      }
    }

    if (!values.UploadFotoKonsumen) {
      errors.UploadFotoKonsumen = 'Foto konsumen harus di upload'
    }
    if (!imageTypeValidate(values.UploadFotoKonsumenType)) {
      errors.UploadFotoKonsumenType = NOTIFY_IMAGE_TYPE
    }
    if (!imageSizeValidate(values.UploadFotoKonsumenSize)) {
      errors.UploadFotoKonsumenSize = NOTIFY_IMAGE_SIZE
    }
    
    return errors
  }
  