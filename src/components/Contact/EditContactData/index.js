import React, { Component } from 'react'
import { reduxForm, Field, change } from 'redux-form'
import { Card, CardBody, CardHeader, Label, Row } from 'reactstrap'
import { connect } from 'react-redux';
import { EditContactVal } from '../../../config/validate/EditContactVal';
import { bindActionCreators } from 'redux';
import { Button, TextField, Grid, Typography } from '@material-ui/core'

const renderField = ({
    input,
    type,
    placeholder,
    label,
    disabled,
    readOnly,
    optional,
    meta: { touched, error, warning }
}) => (
    <Row>
        <Grid md="3"><Label htmlFor="{input}" className="col-form-label">{label}</Label><span><i> {optional}</i></span></Grid>
        <Grid md="7"><TextField {...input} type={type} placeholder={placeholder} disabled={disabled}
        readOnly={
            readOnly}>
        </TextField>
        {touched &&
            ((error && <p style={{ color: 'red' }}>{error}</p>) || (warning && <p style={{ color: 'brown' }}>{warning}</p>))
        }</Grid>
    </Row>
)

const mapStateToProps = (state) => ({
    getContactDetailData: state.ContactReducer.getContactDetailData,
    initialValues: {
        id: state.ContactReducer.getContactDetailData.id ? state.ContactReducer.getContactDetailData.id : "-",
        firstName: state.ContactReducer.getContactDetailData.firstName ? state.ContactReducer.getContactDetailData.firstName : "-",
        lastName: state.ContactReducer.getContactDetailData.lastName ? state.ContactReducer.getContactDetailData.lastName : "-",
        age: state.ContactReducer.getContactDetailData.age ? state.ContactReducer.getContactDetailData.age : "-",
        photo: state.ContactReducer.getContactDetailData.photo ? state.ContactReducer.getContactDetailData.photo : "-",
    },
})

class EditContactData extends Component {
    render() {
        const { handleSubmit} = this.props


        return (
            <Card>
                <form onSubmit={handleSubmit}>
                    <CardHeader style={{lineHeight: "2.3"}}>
                        <Button className="float-right" variant="contained" color="secondary" href={`/`} styles={{margin: '20px'}}>
                            Back
                        </Button>
                    </CardHeader>
                    <CardBody>
                        <h1 style={{textAlign: 'center'}}>EDIT CONTACT</h1>
                        <Grid row className="my-0">
                            <Grid md="12" style={{display: "none"}}>
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="id"
                                    component={renderField}
                                    label="id" 
                                    /> 
                                </Grid>
                            </Grid>
                            <Grid md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="firstName"
                                    component={renderField}
                                    label="First Name" 
                                    /> 
                                </Grid>
                            </Grid>
                            <Grid md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="lastName"
                                    component={renderField}
                                    label="Last Name" /> 
                                </Grid>
                            </Grid>
                            <Grid md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="age"
                                    component={renderField} 
                                    label="Age" /> 
                                </Grid>
                            </Grid>
                            <Grid md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="photo"
                                    component={renderField}
                                    label="Image Url" /> 
                                </Grid>
                            </Grid>
                        </Grid>
                    </CardBody>
                    <CardBody>
                        <Grid row className="my-0">
                            <Grid md="12">
                                <Grid>
                                    <Button 
                                        color="danger" 
                                        type="submit" 
                                        className="float-right">Submit
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </CardBody>
                </form>
            </Card>       
        )
  }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
    }, dispatch);
}

EditContactData = reduxForm({
    form: 'formEditContact',
    validate: EditContactVal,
    enableReinitialize: true
})(EditContactData)

export default connect(mapStateToProps, mapDispatchToProps)(EditContactData)