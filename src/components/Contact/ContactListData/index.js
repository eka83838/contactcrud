import React, { Component } from 'react';
import { connect } from 'react-redux';

import { 
  Card, CardActionArea, CardActions, CardContent, 
  CardMedia, Button, Typography 
} from '@material-ui/core';

import './style.css'

const mapStateToProps = (state) => ({
  getContactData: state.ContactReducer.getContactData,
})

class ContactListData extends Component {

  render() {
    
    const { getContactData } = this.props
    console.log(getContactData)
    return (
      <React.Fragment>
        <Button variant="contained" color="secondary" href={`/add`}>Add New Contact</Button>
        <div>
          <h1 style={{textAlign:'center'}}>List Contact</h1>
          <br/>
          <div className="container-img">
          {
            getContactData &&
              getContactData.map((data, index) => {
                  return(
                      <Card className="card-img" key={index}>
                          <CardActionArea>
                              <CardMedia component="img" style={{height: 140}} src={data.photo}/>
                              <CardContent style={{height: 110, overflow:'hidden'}}>
                                  <Typography gutterBottom variant="h5" component="h2">
                                      {data.firstName} {data.lastName}
                                  </Typography>
                                  <Typography variant="body2" color="textSecondary" component="p">
                                      <strong>age: {data.age}</strong>
                                  </Typography>
                              </CardContent>
                          </CardActionArea>
                          <CardActions>
                              <Button size="small" color="primary" href={`/detail/${data.id}`}> 
                              Detail
                              </Button>
                          </CardActions>
                      </Card>
                  )
              })
          }
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default (connect(mapStateToProps)(ContactListData))
