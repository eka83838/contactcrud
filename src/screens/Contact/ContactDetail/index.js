import React, { Component } from 'react';
import { connect } from 'react-redux';
import ContactDetailData from '../../../components/Contact/ContactDetailData/index'
import { getContactDetail } from '../../../actions/ContactAction'

const mapStateToProps = (state) => ({
})

class ContactDetail extends Component {
  componentDidMount(){
    const { getContactDetail } = this.props
    getContactDetail(this.props.match.params.id)
  }

  render() {
    return (
      <div>
        <ContactDetailData />
      </div>
    )
  }
}

export default (connect(mapStateToProps, { getContactDetail })(ContactDetail))