import React from 'react';
import {Link} from "react-router-dom";
import { ListItem, ListItemIcon } from '@material-ui/core';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';

function Navigation() {

  return (
    <ListItem button>
      <Link to="/">
        <ListItemIcon>
          <PermContactCalendarIcon />
        </ListItemIcon>
        Contact
      </Link>
    </ListItem>
  )
}

export default Navigation