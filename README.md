<h1>Contact CRUD</h1>

<h2>Demo</h2>
View the demo at [https://elastic-williams-9598fd.netlify.app/](https://elastic-williams-9598fd.netlify.app/)

<br/>
<h2>Modul</h2>
Modul for this project is 

<ol>
<li>Create Contact</li>
<li>View/Read Contact</li>
<li>Edit/Update Contact</li>
<li>Delete Contact</li>
<ol>

<br/>

<h2>Setup Project</h2>
How to Setup this Project:

<ul>
<li>git clone https://gitlab.com/widiasereniti/contactcrud.git</li>
<li>cd folder_name</li>
<li>npm install</li>
<li>npm start</li>
</ul>
